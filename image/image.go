package imgdec

import (
	"fmt"
	"golang.org/x/image/webp"
	"image"
	"image/jpeg"
	"image/png"
	"io"
)

var (
	imageDecoders = map[string]func(io.Reader) (image.Image, error){
		"jpg":  jpeg.Decode,
		"jpeg": jpeg.Decode,
		"png":  png.Decode,
		"webp": webp.Decode,
	}
)

func DecodeImage(extension string, reader io.Reader) (img image.Image, err error) {

	decoder, found := imageDecoders[extension]
	if !found {
		return nil, fmt.Errorf("no decoder found for image type: %s", extension)
	}

	return decoder(reader)

}
