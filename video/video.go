package viddec

import (
	"fmt"
	"github.com/rs/xid"
	"gitlab.com/opennota/screengen"
	"image"
	"io"
	"os"
)

// Decode decodes videos into image.Image.
func DecodeVideo(extension string, reader io.Reader) (img image.Image, err error) {

	// FFMPEG wants a local file to open, so we need to save
	// the stream to a file.
	name := fmt.Sprintf("%s.%s", xid.New(), extension)
	path := fmt.Sprintf("/tmp/%s", name)
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return
	}

	defer func() {

		err := file.Close()
		if err != nil {
			return
		}

		err = os.Remove(path)
		if err != nil {
			return
		}

	}()

	_, err = io.Copy(file, reader)
	if err != nil {
		return
	}

	generator, err := screengen.NewGenerator(path)
	if err != nil {
		return
	}

	defer func() {
		err := generator.Close()
		if err != nil {
			return
		}
	}()

	// Take the screenshot in the middle of the video
	frame, err := generator.Image(generator.Duration / 2)
	return frame, err

}

