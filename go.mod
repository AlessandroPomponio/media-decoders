module gitlab.com/AlessandroPomponio/media-decoders

go 1.14

require (
	github.com/rs/xid v1.2.1
	gitlab.com/opennota/screengen v0.0.0-20191122132138-74a8d677a1a5
	golang.org/x/image v0.0.0-20200618115811-c13761719519
)
